# vue-api

Laravel OAUTH2 (API)

## Docs

Auth

- https://medium.com/@mshanak/laravel-5-token-based-authentication-ae258c12cfea

App

- https://github.com/lucadegasperi/oauth2-server-laravel
- https://laravel-news.com/using-vue-router-laravel/

Style Guide

- https://vuetifyjs.com/en/