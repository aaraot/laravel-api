<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');

    Route::resource('users', 'UsersController');
    Route::resource('users-api', 'UsersApiController');
    Route::resource('profile', 'ProfileController');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::post('oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});

Route::get('api/get-ready/{id}', 'ApiService@getReady');
Route::post('api/login', 'ApiService@login');

/*
|--------------------------------------------------------------------------
| Custom API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "oauth" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'oauth'], function () {
    Route::post('api/passwords', 'PasswordService@passwords');
});
