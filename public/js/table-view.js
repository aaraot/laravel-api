/**
 * @param string tableId
 *
 * @return void
 */
$(document).ready(function () {
    // Change position of "Show entries" to ".page-header"
    $('#' + tableId + '_length').insertAfter('.page-header');

    // Set search icon
    var search = '<img src="/public/images/icons/icon_search.svg">';

    // Change position of "Search" to ".page-header"
    $('#' + tableId + '_filter').insertAfter($('#actions').length ? $('#actions') : $('#' + tableId + '_length'));
    // Set placeholder text and change his position
    $('#' + tableId + '_filter').find('input').attr('placeholder', 'Search entries here...').insertAfter('#' + tableId + '_filter label').last();
    // Insert search icon into "Search"
    $('#' + tableId + '_filter').append(search);
    // Remove label "Search:"
    $('#' + tableId + '_filter').find('label').remove();
});