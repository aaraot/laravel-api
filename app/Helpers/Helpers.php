<?php

function pagination($paginate, $page, $array)
{
    $paginate = $paginate;
    $page = $page;

    $offSet = ($page * $paginate) - $paginate;
    $itemsForCurrentPage = array_slice($array, $offSet, $paginate, true);
    $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($array), $paginate, $page);

    return $result->toArray();
}