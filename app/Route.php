<?php

namespace App;

class Route
{
    public $route, $method, $controller, $action, $middleware, $controllerMiddleware;

    // Assigning the values
    public function __construct($route, $method, $controller, $action, $middleware, $controllerMiddleware)
    {
        $this->route = $route;
        $this->method = $method;
        $this->controller = $controller;
        $this->action = $action;
        $this->middleware = $middleware;
        $this->controllerMiddleware = $controllerMiddleware;
    }
}