<?php

namespace App\Http\Controllers;

use App\Mail\ApiRegistration;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class UsersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users-api', ['usersApi' => UserAPI::all(), 'users' => User::whereNotIn('id', UserAPI::get(['id']))->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();

        try {
            $user = User::find($request->get('user_id'));
            $userApi = new UserAPI();

            $userApi->id = $user->id;
            $userApi->secret = str_random(40);
            $userApi->name = $user->email;

            $userApi->save();

            \DB::commit();

            \Mail::to($user->email)->send(new ApiRegistration($user));

            $notification = array(
                'message' => 'A new api user has been created!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->withInput()->with($notification);
        }

        return \Redirect::to('/users-api')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return UserAPI::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();

        try {
            if ($request->get('user_id') !== $id) {
                $user = User::find($request->get('user_id'));
                $userApi = UserAPI::find($id);

                $userApi->id = $user->id;
                $userApi->secret = str_random(40);
                $userApi->name = $user->email;

                $userApi->save();

                \DB::commit();

                $notification = array(
                    'message' => 'User api has been updated!',
                    'alert-type' => 'success'
                );
            } else
                $notification = array(
                    'message' => 'No changes were made!',
                    'alert-type' => 'success'
                );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->withInput()->with($notification);
        }

        return \Redirect::to('/users-api')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            $userApi = UserAPI::find($id);
            $userApi->delete();

            \DB::commit();

            $notification = array(
                'message' => 'User api has been deleted!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }

        return \Redirect::to('/users-api')->with($notification);
    }
}
