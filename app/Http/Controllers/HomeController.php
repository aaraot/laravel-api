<?php

namespace App\Http\Controllers;

use App\Route;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = array();

        foreach (\Route::getRoutes() as $route) {
            if (in_array('oauth', $route->middleware())) {
                $actionName = $route->getActionName();
                $controller = explode('\\', $actionName);
                $controller = isset(explode('@', end($controller))[1]) ? explode('@', end($controller))[1] : '-';
                $action = explode('@', $actionName);
                $action = isset($action[1]) ? $action[1] : '-';

                $_middleware = '';
                $max = count($route->middleware()) - 1;
                foreach ($route->middleware() as $key => $value) {
                    $_middleware .= $value;

                    if ($key !== $max)
                        $_middleware .= ', ';
                }

                $_controllerMiddleware = '';
                $max = count($route->controllerMiddleware());
                if ($max > 0)
                    foreach ($route->controllerMiddleware() as $key => $value) {
                        $_controllerMiddleware .= $value;

                        if ($key !== ($max - 1))
                            $_controllerMiddleware .= ', ';
                    }

                array_push($routes, new Route($route->uri(), $route->methods()[0], $controller, $action, $_middleware, $_controllerMiddleware));
            }
        }

        return view('home', ['routes' => $routes]);
    }
}
