<?php

namespace App\Http\Controllers;

use App\Password;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class ApiService extends Controller
{
    /**
     * Set the user ready by configuring the password
     *
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function getReady($id)
    {
        return view('get-ready', ['id' => $id]);
    }

    /**
     * Login api users into app
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (\Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = User::where('email', $request->get('email'))->first();
            $userApi = UserAPI::where('id', $user->id)->first();

            $credentials = [
                'grant_type' => 'password',
                'client_id' => $userApi->id,
                'client_secret' => $userApi->secret,
                'name' => $user->name,
                'username' => $request->get('email'),
                'password' => $request->get('password')
            ];

            return json_encode($credentials);
        } else
            return response()->json(['error' => 'These credentials do not match our records.'], 403);
    }
}
