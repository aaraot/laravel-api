<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users', ['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'required|unique:users|email'
        ])->validate();

        \DB::beginTransaction();

        try {
            $user = new User();

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = bcrypt('password');

            $user->save();

            $profile = new Profile();

            $profile->user_id = $user->id;

            $profile->save();

            \DB::commit();

            $notification = array(
                'message' => 'A new user has been created!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->withInput()->with($notification);
        }

        return \Redirect::to('/users')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'required|unique:users|email'
        ])->validate();

        \DB::beginTransaction();

        try {
            $user->name = $request->get('name');
            $user->email = $request->get('email');

            $user->save();

            \DB::commit();

            $notification = array(
                'message' => 'User has been updated!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->withInput()->with($notification);
        }

        return \Redirect::to('/users')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        \DB::beginTransaction();

        try {
            $user->delete();

            $userApi = UserAPI::find($user->id);

            if ($userApi)
                $userApi->delete();

            \DB::commit();

            $notification = array(
                'message' => 'User has been deleted!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }

        return \Redirect::to('/users')->with($notification);
    }
}
