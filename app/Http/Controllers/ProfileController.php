<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile', ['user' => \Auth::user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->has('name'))
            \Validator::make($request->all(), [
                'name' => 'required|max:191'
            ])->validate();
        else
            \Validator::make($request->all(), [
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password'
            ])->validate();

        \DB::beginTransaction();

        try {
            $user = User::find(base64_decode($id));

            if ($request->has('name'))
                $user->name = $request->get('name');
            else
                $user->password = bcrypt($request->get('password'));

            $user->save();

            \DB::commit();

            $notification = array(
                'message' => 'Your profile has been updated!',
                'alert-type' => 'success'
            );
        } catch (\Exception $e) {
            \DB::rollback();

            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );

            return back()->withInput()->with($notification);
        }

        if (strpos('users', back()->getTargetUrl()) !== false)
            return \Redirect::to('/profile')->with($notification);
        else
            return \Redirect::to('http://dev.pass-app-api'); // App url
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
