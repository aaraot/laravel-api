<?php

namespace App\Http\Controllers;

use App\Password;
use App\User;
use App\UserAPI;
use Illuminate\Http\Request;

class PasswordService extends Controller
{
    /**
     * Gets all the passwords for the logged user
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function passwords()
    {
        return json_encode(Password::whereIn('user', [\LucaDegasperi\OAuth2Server\Facades\Authorizer::getResourceOwnerId()])->get());
    }
}
