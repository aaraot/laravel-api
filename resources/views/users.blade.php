@extends('layouts.api')

@section('css')
    <link href="{{ asset('/public/plugins/dataTables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section id="tableView">
        <div class="header">
            <h2 class="page-header title">Users</h2>

            {{-- Actions --}}
            <div id="actions" class="pull-right">
                <button type="button" class="btn btn-new" data-toggle="modal" data-target="#userModal">
                    New
                </button>
            </div>
            {{-- End Actions --}}
        </div>

        <div class="table-responsive">
            {{-- Users Table --}}
            <table class="table table-striped" id="tableUsers">
                {{-- Header --}}
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                {{-- End Header --}}

                {{-- Body --}}
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ explode(' ', $user->created_at)[0] }}</td>
                        {{-- Actions --}}
                        <td>
                            {{-- Edit --}}
                            <img src="{{ url('public/images/icons/icon_edit.svg') }}" title="Edit User"
                                 id="editUser{{ $user->id }}"
                                 user-id="{{ $user->id }}">
                            {{-- End Edit --}}

                            {{-- Delete --}}
                            <img src="{{ url('public/images/icons/icon_delete.svg') }}" title="Delete User"
                                 id="deleteUser{{ $user->id }}" user-id="{{ $user->id }}"
                                 data-toggle="modal" data-target="#deleteModal" data-id="{{ $user->id }}"
                                 data-name="{{ $user->name }}">
                            {{-- End Delete --}}
                        </td>
                        {{-- Actions --}}
                    </tr>
                @endforeach
                </tbody>
                {{-- End Body --}}
            </table>
            {{-- End Users Table --}}
        </div>
    </section>

    <section id="parentUserModal">
        @include('components.datatable.new-user')
    </section>

    <section id="parentDeleteModal">
        @include('components.datatable.delete-user')
    </section>
@endsection

@section('js')
    <script src="{{ asset('/public/plugins/dataTables/jquery.dataTables.js') }}"></script>

    <script>
        var tableId = 'tableUsers';
        var $dataTableId = '#tableUsers';
        var $modalTitle = 'Edit User';
        var $modalUrl = '/users/';
        var $deleteMessage = 'user';
        var $old = false;
        var $newUser = $('#parentUserModal').html();
        var $modalId = '#userModal';

        function editModal() {
            // Remove attribute required from password input
            $($modalId + ' #password').removeAttr('required');
        }
    </script>

    @include('components.js.datatable-view')
    <script src="{{ asset('/public/js/table-view.js') }}"></script>
@endsection