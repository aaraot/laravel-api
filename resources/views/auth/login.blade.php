@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="blur-container">
            <div class="blur"></div>
            <div class="panel panel-default">
                <div class="panel-heading title">Login to your account</div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        {{-- E-mail --}}
                        <div class="form-group {{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                                   placeholder="E-mail" required>

                            @if ($errors->has('email'))
                                @include('components.error', ['errors' => $errors->get('email')])
                            @endif
                        </div>
                        {{-- End E-mail --}}

                        {{-- Remember Me --}}
                        <div class="form-group">
                            <div class="checkbox">
                                <label class="squaredOne">
                                    <input type="checkbox" id="squaredOne"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="squaredOne"> Remember Me</label>
                                </label>
                            </div>
                        </div>
                        {{-- End Remember Me --}}

                        {{-- Password --}}
                        <div class="form-group {{ $errors->has('password') ? ' has-error has-feedback' : '' }}">
                            <label for="password">Password</label>

                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Password"
                                   required>

                            @if ($errors->has('password'))
                                @include('components.error', ['errors' => $errors->get('password')])
                            @endif
                        </div>
                        {{-- End Password --}}

                        {{-- Forgot Password --}}
                        <div class="form-group">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot my password?
                            </a>
                        </div>
                        {{-- End Forgot Password --}}

                        {{-- Actions --}}
                        <div class="form-group">
                            <div class="pull-right">
                                @if (\App\User::count() === 0)
                                    <a class="btn btn-link" href="{{ route('register') }}" style="margin-right: 20px">
                                        I don't have an account
                                    </a>
                                @endif

                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                        {{-- Actions --}}
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection