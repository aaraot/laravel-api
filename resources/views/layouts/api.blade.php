<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'API') }}</title>

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('/public/frameworks/bootstrap-3.3.7/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/frameworks/bootstrap-3.3.7/css/bootstrap-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/frameworks/bootstrap-3.3.7/css/theme/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/app.css') }}" rel="stylesheet">

    <!-- Plugins -->
    <link href="{{ asset('/public/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Theme -->
    <link href="{{ asset('/public/css/theme.css') }}" rel="stylesheet">

    <!-- Page CSS -->
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a href="{{ url('/') }}">
                    <img src="{{ asset('/public/images/logo.png') }}" width="70">
                </a>
            </div>

            {{-- Menu --}}
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    @php
                        $menus = [
                            ['url' => '/', 'text' => 'Overview'],
                            ['url' => 'users', 'text' => 'Users'],
                            ['url' => 'users-api', 'text' => 'API Users']
                        ];
                    @endphp

                    @foreach($menus as $menu)
                        <li class="@if(Request::is($menu['url'])) active @endif">
                            <a href="{{ url($menu['url']) }}">{{ $menu['text'] }}</a>
                        </li>
                    @endforeach
                </ul>

                {{-- User Menu --}}
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="{{ asset('/public/images/icons/icon_user.svg') }}">
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li class="@if(Request::is('profile')) active @endif">
                                <a href="{{ url('profile')  }}">Profile</a>
                            </li>

                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 80px">
        <div class="panel panel-default @if(Request::is('profile')) panel-half @endif">
            <div class="panel-body">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/public/frameworks/jquery-1.12.4/jquery.min.js') }}"></script>
<script src="{{ asset('/public/frameworks/bootstrap-3.3.7/js/bootstrap.js') }}"></script>

<!-- Plugins -->
<script src="{{ asset('/public/plugins/toastr/toastr.min.js') }}"></script>

<!-- Page Scripts -->
@yield('js')

<script>
    @if(Session::has('message'))
        var type = '{{ Session::get('alert-type', 'info') }}';

        switch (type) {
            case 'info':
                toastr.info('{{ Session::get('message') }}');
                break;
            case 'warning':
                toastr.warning('{{ Session::get('message') }}');
                break;
            case 'success':
                toastr.success('{{ Session::get('message') }}');
                break;
            case 'error':
                toastr.error('{{ Session::get('message') }}');
                break;
        }
    @endif
</script>

</body>
</html>
