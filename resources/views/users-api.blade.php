@extends('layouts.api')

@section('css')
    <link href="{{ asset('/public/plugins/dataTables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section id="tableView">
        <div class="header">
            <h2 class="page-header title">Users API</h2>

            {{-- Actions --}}
            <div id="actions" class="pull-right">
                <button type="button" class="btn btn-new" data-toggle="modal" data-target="#userApiModal">
                    New
                </button>
            </div>
            {{-- End Actions --}}
        </div>

        <div class="table-responsive">
            {{-- Users API Table --}}
            <table class="table table-striped" id="tableUsersApi">
                {{-- Header --}}
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                {{-- End Header --}}

                {{-- Body --}}
                <tbody>
                @foreach($usersApi as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ explode(' ', $user->created_at)[0] }}</td>
                        {{-- Actions --}}
                        <td>
                            {{-- Edit --}}
                            <img src="{{ url('public/images/icons/icon_edit.svg') }}" title="Edit User" id="editUser{{ $user->id }}"
                                 user-id="{{ $user->id }}">
                            {{-- End Edit --}}

                            {{-- Delete --}}
                            <img src="{{ url('public/images/icons/icon_delete.svg') }}" title="Delete User"
                                 id="deleteUser{{ $user->id }}" user-id="{{ $user->id }}"
                                 data-toggle="modal" data-target="#deleteModal" data-id="{{ $user->id }}"
                                 data-name="{{ $user->name }}">
                            {{-- End Delete --}}
                        </td>
                        {{-- Actions --}}
                    </tr>
                @endforeach
                </tbody>
                {{-- End Body --}}
            </table>
            {{-- End Users API Table --}}
        </div>
    </section>

    <section id="parentUserModal">
        @include('components.datatable.new-user-api')
    </section>

    <section id="parentDeleteModal">
        @include('components.datatable.delete-user')
    </section>
@endsection

@section('js')
    <script src="{{ asset('/public/plugins/dataTables/jquery.dataTables.js') }}"></script>

    <script>
        var tableId = 'tableUsersApi';
        var $dataTableId = '#tableUsersApi';
        var $modalTitle = 'Edit User API';
        var $modalUrl = '/users-api/';
        var $deleteMessage = 'user api';
        var $old = false;
        var $newUser = $('#parentUserModal').html();
        var $modalId = '#userApiModal';

        function editModal() {
        }
    </script>

    @include('components.js.datatable-view')
    <script src="{{ asset('/public/js/table-view.js') }}"></script>
@endsection