<div id="pagination">
    {{-- Total rows --}}
    <span class="pull-left">
            Showing {{ $obj['from'] }} to {{ $obj['to'] }} of {{ $obj['total'] }} entries
        </span>

    {{-- Pagination --}}
    <ul class="pagination pull-right">
        {{-- Prev --}}
        <li class="@if($obj['prev_page_url'] === null) disabled @endif">
            @if($obj['prev_page_url'] === null)
                <span>«</span>
            @else
                <a href="{{ url($obj['prev_page_url']) }}" rel="next">«</a>
            @endif
        </li>

        {{-- Pages --}}
        @for($i = 1; $i <= $obj['last_page']; $i++)
            <li class="@if($i === $obj['current_page']) active @endif">
                <a href="{{ url('?page=' . $i) }}">{{ $i }}</a>
            </li>
        @endfor

        {{-- Next --}}
        <li class="@if($obj['next_page_url'] === null) disabled @endif">
            @if($obj['next_page_url'] === null)
                <span>»</span>
            @else
                <a href="{{ url($obj['next_page_url']) }}" rel="next">»</a>
            @endif
        </li>
    </ul>
</div>