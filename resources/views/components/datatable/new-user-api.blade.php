{{-- User Modal --}}
<div class="modal fade" id="userApiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{-- Form --}}
            <form action="{{ route('users-api.store') }}" method="post">
                {{-- Header --}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h4 class="modal-title title" id="myModalLabel">New API User</h4>
                </div>
                {{-- End Header --}}

                {{-- Body --}}
                <div class="modal-body">
                    <section id="form">
                        {{-- Id --}}
                        <input type="hidden" name="id" id="id" value="">
                        {{-- End Id --}}

                        {{-- Users --}}
                        <div class="form-group">
                            <label for="userId">User</label>
                            <select class="form-control" id="userId" name="user_id" placeholder="Select a user"
                                    required>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- End Users --}}
                    </section>
                </div>
                {{-- End Body --}}

                {{-- Footer --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
                {{-- Footer --}}
            </form>
            {{-- End Form --}}
        </div>
    </div>
</div>
{{-- End User Modal --}}