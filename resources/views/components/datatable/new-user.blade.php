{{-- User Modal --}}
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{-- Form --}}
            <form action="{{ route('users.store') }}" method="post">
                {{-- Header --}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ url('public/images/icons/icon_cross.svg') }}">
                    </button>

                    <h4 class="modal-title title" id="myModalLabel">New User</h4>
                </div>
                {{-- End Header --}}

                {{-- Body --}}
                <div class="modal-body">
                    <section id="form">
                        {{-- Id --}}
                        <input type="hidden" name="id" id="id" value="">
                        {{-- End Id --}}

                        {{-- Name --}}
                        <div class="form-group {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                                   placeholder="Name" required>

                            @if ($errors->has('name'))
                                @include('components.error', ['errors' => $errors->get('name')])
                            @endif
                        </div>
                        {{-- End Name --}}

                        {{-- E-mail --}}
                        <div class="form-group {{ $errors->has('email') ? ' has-error has-feedback' : '' }}">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                                   placeholder="E-mail"
                                   required>

                            @if ($errors->has('email'))
                                @include('components.error', ['errors' => $errors->get('email')])
                            @endif
                        </div>
                        {{-- End E-mail --}}
                    </section>
                </div>
                {{-- End Body --}}

                {{-- Footer --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
                {{-- Footer --}}
            </form>
            {{-- End Form --}}
        </div>
    </div>
</div>
{{-- End User Modal --}}