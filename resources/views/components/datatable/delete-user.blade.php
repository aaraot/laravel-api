<div class="modal fade bs-example-modal-sm" id="deleteModal" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            {{-- Header --}}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ url('public/images/icons/icon_cross.svg') }}">
                </button>

                <h4 class="modal-title title" id="myModalLabel"></h4>
            </div>
            {{-- End Header --}}

            {{-- Body --}}
            <div class="modal-body">
                <section id="deleteInfo">

                </section>
            </div>
            {{-- End Body --}}

            {{-- Footer --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                <form action="" method="post"
                      style="display: inline-block">
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-primary btn-delete">Delete</button>
                </form>
            </div>
            {{-- Footer --}}
        </div>
    </div>
</div>