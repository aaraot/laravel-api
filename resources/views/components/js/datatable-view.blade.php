<script>
    /**
     * Datatable view required configurations
     *
     * @param string $dataTableId
     * @param string $modalTitle
     * @param string $modalUrl
     * @param string $deleteMessage
     * @param boolean $old
     * @param element $newUser
     * @param string $modalId
     * @function editModal()
     */

    $(document).ready(function () {
        // Init dataTable
        $($dataTableId).DataTable({
            columnDefs: [
                {orderable: false, targets: -1}
            ]
        });

        // Check if view has old inputs
        @if (old())
            $old = true;
        @endif

        // If $old true
        if ($old) {
            // Set modal with the proper configs
            if ("{{ old('id') }}" !== '')
                setModal(true, $modalTitle, true, <?= json_encode(old()); ?>);
            else
                setModal(false, '', true, <?= json_encode(old()); ?>);
        }

        // On edit
        $('[id^="editUser"]').click(function () {
            // Get the user info
            $.get($modalUrl + $(this).attr('user-id') + '/edit', function (data) {
                setModal(true, $modalTitle, false, data);
            });
        });

        // On delete modal open
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = {'id': button.data('id'), 'name': button.data('name')} // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            $('.modal-header .title').text('Deleting ' + recipient.name);
            modal.find('.modal-body #deleteInfo').html('You are about to delete <span class="delete-name"> ' + recipient.name + '</span>. ' +
                'Deleting this user is permanent and you cannot undo this action. <br><br> Are you sure yow want to delete the ' + $deleteMessage + '<span class="delete-name"> ' + recipient.name + '</span>?');
            modal.find('.modal-footer form').attr('action', $modalUrl + recipient.id);
        });

        // On modal close
        $('body').on('hidden.bs.modal', function () {
            // Reset modal
            $('#parentUserModal').html($newUser);
            // Remove errors
            $('#parentUserModal .form-group').each(function () {
                $(this).removeClass('has-error has-feedback');
                $(this).find('.errors').remove();
                $(this).find('.form-control-feedback').remove();
            });
        });
    });

    /**
     * Configure the modal depending on the action clicked
     *
     * @param boolean edit
     * @param string title
     * @param boolean error
     * @param object/array data
     *
     * @return void
     */
    function setModal(edit, title, error, data) {
        // Open modal
        $($modalId).modal('toggle');

        // Check if we are editing a user
        if (edit) {
            // Set the form action
            $('#userModal form').attr('action', $modalUrl + data.id).append('{{ method_field('PUT') }}');
            // Set the modal title
            $('#userModal h4').html(title);

            editModal();

            // Set the submit button text
            $('#userModal :submit').html('Update');
        }

        // Check if modal has errors
        if (error) {
            // Populate the modal inputs with the information sent
            @foreach(old() as $key => $value)
                $($modalId + ' #' + '{{ $key }}').val('{{ $value }}');
            @endforeach
        }
        else
        // Populate the modal inputs with the information requested
            $($modalId + ' input').each(function () {
                if ($(this).attr('id'))
                    $(this).val(data[$(this).attr('id')]);
            });
    }
</script>