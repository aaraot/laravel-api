@extends('layouts.api')

@section('content')
    <section id="profilePage">
        <h1 class="page-header title">Profile</h1>

        <div class="row">
            <div class="col-md-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"
                        class="{{ old('tab') === 'profile' ? 'active' : (old('tab') === null ? 'active' : '') }}">
                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a>
                    </li>

                    <li role="presentation" class="@if (old('tab') === 'password') active @endif">
                        <a href="#password" aria-controls="password" role="tab" data-toggle="tab">Change Password</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    {{-- Profile --}}
                    <div role="tabpanel"
                         class="tab-pane {{ old('tab') === 'profile' ? 'active' : (old('tab') === null ? 'active' : '') }}"
                         id="profile">
                        {{-- Form --}}
                        <form action="{{ route('profile.update', base64_encode($user->id)) }}" method="post">
                            {{ method_field('PUT') }}

                            <input type="hidden" name="tab" value="profile">

                            {{-- Name --}}
                            <div class="form-group {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                       value="{{ old('name') ?: $user->name }}" required>

                                @if ($errors->has('name'))
                                    @include('components.error', ['errors' => $errors->get('name')])
                                @endif
                            </div>
                            {{-- End Name --}}

                            {{-- Actions --}}
                            <section id="actions" class="pull-right">
                                <button type="button" class="btn btn-default"
                                        onclick="window.location='{{ url('/') }}'">Cancel
                                </button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </section>
                            {{-- End Actions --}}
                        </form>
                        {{-- End Form --}}
                    </div>
                    {{-- End Profile --}}

                    {{-- Change Password --}}
                    <div role="tabpanel" class="tab-pane @if (old('tab') === 'password') active @endif" id="password">
                        {{-- Form --}}
                        <form action="{{ route('profile.update', base64_encode($user->id)) }}" method="post">
                            {{ method_field('PUT') }}

                            <input type="hidden" name="tab" value="password">

                            {{-- Password --}}
                            <div class="form-group {{ $errors->has('password') ? ' has-error has-feedback' : '' }}">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Password" required>

                                @if ($errors->has('password'))
                                    @include('components.error', ['errors' => $errors->get('password')])
                                @endif
                            </div>
                            {{-- End Password --}}

                            {{-- Confirm Password --}}
                            <div class="form-group {{ $errors->has('confirm-password') ? ' has-error has-feedback' : '' }}">
                                <label for="confirmPassword">Confirm Password</label>
                                <input type="password" class="form-control" id="confirmPassword"
                                       name="confirm_password" placeholder="Confirm Password" required>

                                @if ($errors->has('confirm-password'))
                                    @include('components.error', ['errors' => $errors->get('confirm-password')])
                                @endif
                            </div>
                            {{-- End Confirm Password --}}

                            {{-- Actions --}}
                            <section id="actions" class="pull-right">
                                <button type="button" class="btn btn-default"
                                        onclick="window.location='{{ url('/') }}'">
                                    Cancel
                                </button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </section>
                            {{-- End Actions --}}
                        </form>
                        {{-- End Form --}}
                    </div>
                    {{-- End Change Password --}}
                </div>
            </div>
        </div>
    </section>
@endsection
