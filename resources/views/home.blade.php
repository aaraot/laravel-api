@extends('layouts.api')

@section('css')
    <link href="{{ asset('/public/plugins/dataTables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    {{-- Routes --}}
    <section id="tableView">
        {{-- Header --}}
        <div class="header">
            <h2 class="page-header title">Routes</h2>
        </div>
        {{-- End Header --}}

        {{-- Table Routes --}}
        <div class="table-responsive">
            <table class="table table-striped" id="tableRoutes">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Route</th>
                    <th>Method</th>
                    <th>Controller</th>
                    <th>Action</th>
                    <th>Middleware (Route)</th>
                    <th>Middleware (Controller)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($routes as $route)
                    <tr>
                        <td>#</td>
                        <td>{{ $route->route }}</td>
                        <td>{{ $route->method }}</td>
                        <td>{{ $route->controller }}</td>
                        <td>{{ $route->action }}</td>
                        <td>{{ $route->middleware }}</td>
                        <td>{{ $route->controllerMiddleware }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{-- End Table Routes --}}
    </section>
    {{-- End Routes --}}
@endsection

@section('js')
    <script src="{{ asset('/public/plugins/dataTables/jquery.dataTables.js') }}"></script>

    <script>
        var tableId = 'tableRoutes';

        $(document).ready(function () {
            // Init dataTable
            $('#tableRoutes').DataTable();
        });
    </script>

    <script src="{{ asset('/public/js/table-view.js') }}"></script>
@endsection
