@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="blur-container">
            <div class="blur"></div>
            <div class="panel panel-default">
                <div class="panel-heading">Set a password</div>
                <div class="panel-body">
                    {{-- Form --}}
                    <form action="{{ route('profile.update', $id) }}" method="post">
                        {{ method_field('PUT') }}

                        <input type="hidden" name="tab" value="password">

                        {{-- Password --}}
                        <div class="form-group {{ $errors->has('password') ? ' has-error has-feedback' : '' }}">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Password" required>

                            @if ($errors->has('password'))
                                @include('components.error', ['errors' => $errors->get('password')])
                            @endif
                        </div>
                        {{-- End Password --}}

                        {{-- Confirm Password --}}
                        <div class="form-group {{ $errors->has('confirm-password') ? ' has-error has-feedback' : '' }}">
                            <label for="confirmPassword">Confirm Password</label>
                            <input type="password" class="form-control" id="confirmPassword"
                                   name="confirm_password" placeholder="Confirm Password" required>

                            @if ($errors->has('confirm-password'))
                                @include('components.error', ['errors' => $errors->get('confirm-password')])
                            @endif
                        </div>
                        {{-- End Confirm Password --}}

                        {{-- Actions --}}
                        <section id="actions" class="pull-right">
                            <button type="submit" class="btn btn-primary">Setup</button>
                        </section>
                        {{-- End Actions --}}
                    </form>
                    {{-- End Form --}}
                </div>
            </div>
        </section>
    </div>
@endsection
