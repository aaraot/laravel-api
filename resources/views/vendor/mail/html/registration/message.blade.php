@component('mail::registration.layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::registration.header', ['src' => asset('/public/images/mail/logo.png')])
        @endcomponent
    @endslot

    {{-- Body --}}
    @slot('body')
        @component('mail::registration.body', ['src' => asset('/public/images/mail/icon.svg'), 'name' => $user->name])
            @slot('button')
                @component('mail::registration.button', ['url' => url('/api-service/get-ready/' . base64_encode($user->id))])
                    Visit Website
                @endcomponent
            @endslot
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::registration.footer')
            © {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
