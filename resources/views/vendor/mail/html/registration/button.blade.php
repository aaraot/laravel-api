<a href="{{ $url }}" class="btn btn-default" target="_blank">
    <span class="background">{{ $slot }}</span>
</a>