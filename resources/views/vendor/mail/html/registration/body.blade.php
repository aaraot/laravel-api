<tbody>
    <tr>
        <td class="background banner">
            <img src="{{ $src }}">

            {{ $button }}
        </td>
    </tr>

    <tr>
        <td>
            <h1>Hello {{ $name }}!</h1>

            <p>You are almost ready to start enjoying API.</p>
            <p>Simply click the button above to try out our product!</p>
        </td>
    </tr>
</tbody>